import styled from "styled-components";

export const Title = styled.h1`
font-size:${props =>`${props.fontSize}px`};
color:${props => props.colorPrimary};
`
