import styled from "styled-components";



export const Container = styled.div`
width: 100%;
gap:${ props => `${props.gapSet}px`};
align-items: ${props => props.aligContainerSet};
justify-content:${ props => props.justifyContainerSet};
display:${props => props.displaySet};
flex-direction: ${props => props.flexDirectionSet};


& > a{
  padding: 10px;
  
}
`

export const LiksStyle = styled.a`
text-decoration: none;
font-size: ${props => `${props.linkFontSizeSet}px`};
color:${props => props.linkColor};
background-color: ${props => props.backgrondColorLinkSet};
&:hover{
  background-color: ${props => props.backgrondColorLinkHoverSet};
}
`




//=====================================================================================

import React from 'react'
import * as C from "./style";

export const Nav = () =>{
 
  return(
   <C.Container 
   alignContainerSet={"center"}
   justifyContainerSet={"center"}
   displaySet={"flex"} 
   gapSet={"10"}
   flexDirectionSet={"row"}>
       <C.LiksStyle
         backgrondColorLinkHoverSet={"#a2a1a3"} 
         backgrondColorLinkSet={"#3daca4"} 
        linkFontSizeSet={"30"}
         linkColorSet={"#4ac212"
         }>Home</C.LiksStyle>

        <C.LiksStyle
         backgrondColorLinkHoverSet={"#a2a1a3"} 
         backgrondColorLinkSet={"#3daca4"} 
        linkFontSizeSet={"30"}
         linkColorSet={"#4ac212"
         }>Sobre</C.LiksStyle>

        <C.LiksStyle
         backgrondColorLinkHoverSet={"#a2a1a3"} 
         backgrondColorLinkSet={"#3daca4"} 
        linkFontSizeSet={"30"}
         linkColorSet={"#4ac212"
         }>Projetos</C.LiksStyle>

        <C.LiksStyle
         backgrondColorLinkHoverSet={"#a2a1a3"} 
         backgrondColorLinkSet={"#3daca4"} 
        linkFontSizeSet={"30"}
         linkColorSet={"#4ac212"
         }>Liguagems</C.LiksStyle>


        <C.LiksStyle
         backgrondColorLinkHoverSet={"#a2a1a3"} 
         backgrondColorLinkSet={"#3daca4"} 
        linkFontSizeSet={"30"}
         linkColorSet={"#4ac212"
         }>Contato</C.LiksStyle>

   </C.Container>
  )
}
